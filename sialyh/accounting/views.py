from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from .forms import *
from .models import *


# Create your views here.

@login_required
def accountingView(request):
    return render (request, 'accounting/accounting.html')

@login_required
def income_accounting_view(request):
    return render (request, 'accounting/income_pettycash.html')

@login_required
def listings_accounting_view(request):
    return render (request, 'accounting/list_pettycash.html')



@login_required
def petty_cashView(request):
    if request.method == 'POST':
        formpc = form_petty_cash(request.POST, request.FILES)
        if formpc.is_valid():
            petty_cash = formpc.save(commit=False, user=request.user)
            petty_cash.user = request.user
            petty_cash.save()
            messages.success(request, 'Información enviada correctamente.')
            return redirect('petty_cash')
    else:
        formpc = form_petty_cash()
    return render (request, 'petty_cash/petty_cash.html', {'formpc':formpc})


@login_required
def petty_cash_list(request):
    miFormpc = petty_cash.objects.all()

    date_invoice_filter_formpc = date_filterFormpc(request.GET)
    user_filter_formpc = user_filterFormpc(request.GET)

    if date_invoice_filter_formpc.is_valid() and user_filter_formpc.is_valid():
        datepc = date_invoice_filter_formpc.cleaned_data.get('dateInvoice')
        userpc = user_filter_formpc.cleaned_data.get('user')

        if datepc:
            miFormpc = miFormpc.filter(dateInvoice = datepc)
        if userpc:
            miFormpc = miFormpc.filter(user__username = userpc)

        return render (request, 'petty_cash/petty_cash_list.html', {
            'miFormpc':miFormpc,
            'date_invoice_filter_formpc':date_invoice_filter_formpc,
            'user_filter_formpc':user_filter_formpc,
        })
    else:
        return render (request, 'petty_cash/petty_cash_list.html', {
            'miFormpc':miFormpc,
            'date_invoice_filter_formpc':date_invoice_filter_formpc,
            'user_filter_formpc':user_filter_formpc,
        })


@login_required
def disable_item_petty_cash(request, invoice_id):
    pettycash = petty_cash.objects.get(id=invoice_id)
    pettycash.enabled = not pettycash.enabled
    pettycash.save()
    return redirect('petty_cash_list')