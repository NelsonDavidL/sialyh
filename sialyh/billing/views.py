from django.shortcuts import render, redirect
from .forms import *
from django.contrib.auth.decorators import login_required
from .models import *
from django.contrib import messages
from django.views.generic import UpdateView
from django.urls import reverse_lazy
from django.http import HttpResponse, JsonResponse
from openpyxl import *
from datetime import datetime
from sialyhapp.functions import *


# Create your views here.

# ACA VAN LAS VISTAS PARA LOS MODULOS DE INGRESOS Y LISTADOS

# Esta es la vista de la seccion de facturacion en el Home
@login_required
def billingView(request):
    return render(request, "billing/billing.html")


# Esta es la vista que renderiza las opciones para los ingresos
@login_required
def incomeView(request):
    return render(request, "billing/income.html")


# Esta es la vista que renderiza las opciones para los listados
@login_required
def listingsView(request):
    return render(request, "billing/listings.html")


# ACA VAN LAS VISTAS DE LOS FORMULARIOS DE INGRESO DE DOCUMENTOS

# Esta es la vista del formulario de ingreso de facturas manuales
@login_required
def manual_invoicesView(request):
    if request.method == 'POST':
        form = form_manual_billing(request.POST, request.FILES)
        if form.is_valid():
            man_invoice = form.save(commit=False, user=request.user)
            man_invoice.user = request.user
            man_invoice.save()
            messages.success(request, 'Información enviada correctamente.')
            return redirect('manual_invoices')
    else:
        form = form_manual_billing()
    return render(request, 'manual_invoices/manual_invoices.html', {'form': form})


# Esta es la vista del formulario de ingreso de bonos de separado
@login_required
def sep_bonusesView(request):
    if request.method == 'POST':
        formsb = form_separated_bonuses(request.POST, request.FILES)
        if formsb.is_valid():
            separated_bond = formsb.save(commit=False, user=request.user)
            separated_bond.user = request.user
            separated_bond.save()
            messages.success(request, 'Información enviada correctamente.')
            return redirect('sepBonuses')
    else:
        formsb = form_separated_bonuses()
    return render(request, 'separate_bonuses/separate_bonuses.html', {'formsb': formsb})


# Esta es la vista del formulario de ingreso de descuentos por nomina
@login_required
def pay_discountsView(request):
    if request.method == 'POST':
        formpp = form_payroll_discount(request.POST, request.FILES)
        if formpp.is_valid():
            payroll_discount = formpp.save(commit=False, user=request.user)
            payroll_discount.user = request.user
            payroll_discount.save()
            messages.success(request, 'Información enviada correctamente.')
            return redirect('payDiscounts')
    else:
        formpp = form_payroll_discount()
    return render(request, 'payroll_discounts/payroll_discounts.html', {'formpp': formpp})


#Esta es la vista del formulario de ingreso de manejo de datos
@login_required
def data_managementView(request):
    # Lógica para el formulario de envío de datos
    if request.method == 'POST':
        formdm = form_data_management(request.POST, request.FILES)
        if formdm.is_valid():
            data_management = formdm.save(commit=False, user=request.user)
            data_management.user = request.user
            data_management.save()
            messages.success(request, 'Información enviada correctamente.')
            return redirect('dataManagement')
    else:
        formdm = form_data_management()
    return render(request, 'data_management/data_management.html', {'formdm': formdm})


# ACA VAN LAS VISTAS DE LOS LISTADOS DE LOS DOCUMENTOS INGRESADOS

# Esta es la vista del listado de facturas manuales
@login_required
def invoice_list(request):
    miForm = manInvoice.objects.all()
    # Aca se inicializan los formularios de filtro
    date_invoice_filter_form = invoice_date_filterForm(request.GET)
    user_filter_form = user_filterForm(request.GET)
    # Aca se verifica si los formularios son válidos
    if date_invoice_filter_form.is_valid() and user_filter_form.is_valid():
        date_invoice = date_invoice_filter_form.cleaned_data.get('dateInvoice')
        user_filter = user_filter_form.cleaned_data.get('user')
        # Aca se filtra el queryset de manInvoice
        if date_invoice:
            miForm = miForm.filter(dateInvoice=date_invoice)
        if user_filter:
            miForm = miForm.filter(user__username=user_filter)
        # Aca se renderiza la plantilla con los objetos filtrados y los formularios de filtro
        return render(request, 'manual_invoices/invoices.html', {
            'miForm': miForm,
            'date_invoice_filter_form': date_invoice_filter_form,
            'user_filter_form': user_filter_form,
            'filtered_date': date_invoice,
            'filtered_user': user_filter,
        })
    else:
        return render(request, 'manual_invoices/invoices.html', {
            'miForm': miForm,
            'date_invoice_filter_form': date_invoice_filter_form,
            'user_filter_form': user_filter_form,
        })


# Esta es la vista del listado de bonos de separado
@login_required
def sepBonuses_list(request):
    miFormsb = sepBonuses.objects.all()
    # Aca se inicializan los formularios de filtro
    date_invoice_filter_formsb = invoice_date_filterFormsb(request.GET)
    user_filter_formsb = user_filterFormsb(request.GET)
    # Aca se verifica si los formularios son válidos
    if date_invoice_filter_formsb.is_valid() and user_filter_formsb.is_valid():
        date_invoice = date_invoice_filter_formsb.cleaned_data.get('dateInvoice')
        user_filter = user_filter_formsb.cleaned_data.get('user')
        # Aca se filtra el queryset de sepBonuses
        if date_invoice:
            miFormsb = miFormsb.filter(dateInvoice=date_invoice)
        if user_filter:
            miFormsb = miFormsb.filter(user__username=user_filter)
        return render(request, 'separate_bonuses/separate_bonuses_list.html', {
            'miFormsb': miFormsb,
            'date_invoice_filter_formsb': date_invoice_filter_formsb,
            'user_filter_formsb': user_filter_formsb,
            'filtered_date': date_invoice,
            'filtered_user': user_filter,
        })
    else:
        # Aca se renderiza la plantilla con los objetos filtrados y los formularios de filtro
        return render(request, 'separate_bonuses/separate_bonuses_list.html', {
            'miFormsb': miFormsb,
            'date_invoice_filter_formsb': date_invoice_filter_formsb,
            'user_filter_formsb': user_filter_formsb, })


# Esta es la vista del listado de los descuentos por nómina
@login_required
def payDiscounts_list(request):
    miFormpd = payDiscounts.objects.all()
    # Aca se inicializan los formularios de filtro
    date_invoice_filter_formpd = invoice_date_filterFormpd(request.GET)
    user_filter_formpd = user_filterFormpd(request.GET)
    # Aca se verifica si los formularios son válidos
    if date_invoice_filter_formpd.is_valid() and user_filter_formpd.is_valid():
        date_invoice = date_invoice_filter_formpd.cleaned_data.get('datePayDiscounts')
        user_filter = user_filter_formpd.cleaned_data.get('user')
        # Aca se filtra el queryset de sepBonuses
        if date_invoice:
            miFormpd = miFormpd.filter(datePayDiscounts=date_invoice)
        if user_filter:
            miFormpd = miFormpd.filter(user__username=user_filter)
        return render(request, 'payroll_discounts/payroll_discounts_list.html', {
            'miFormpd': miFormpd,
            'date_invoice_filter_formpd': date_invoice_filter_formpd,
            'user_filter_formpd': user_filter_formpd,
            'filtered_date': date_invoice,
            'filtered_user': user_filter,
        })
    # Aca se renderiza la plantilla con los objetos filtrados y los formularios de filtro
    return render(request, 'payroll_discounts/payroll_discounts_list.html', {
        'miFormpd': miFormpd,
        'date_invoice_filter_formpd': date_invoice_filter_formpd,
        'user_filter_formpd': user_filter_formpd, })


# Esta es la vista del listado de manejo de datos personales
@login_required
def dataManagement_list(request):
    miFormdm = dataManagement.objects.all()
    # Aca se inicializan los formularios de filtro
    date_filter_formdm = date_filterFormdm(request.GET)
    user_filter_formdm = user_filterFormdm(request.GET)

    # Aca se verifica si los formularios son válidos
    if date_filter_formdm.is_valid() and user_filter_formdm.is_valid():
        date = date_filter_formdm.cleaned_data.get('date')
        user = user_filter_formdm.cleaned_data.get('user')

        # Aca se filtra el queryset de sepBonuses
        if date:
            miFormdm = miFormdm.filter(created=date)
        if user:
            miFormdm = miFormdm.filter(user__username=user)

        return render(request, 'data_management/data_management_list.html', {
            'miFormdm': miFormdm,
            'date_filter_formdm': date_filter_formdm,
            'user_filter_formdm': user_filter_formdm,
            'filtered_date': date,
            'filtered_user': user,
        })
    # Aca se renderiza la plantilla con los objetos filtrados y los formularios de filtro
    return render(request, 'data_management/data_management_list.html', {
        'miFormdm': miFormdm,
        'date_filter_formdm': date_filter_formdm,
        'user_filter_formdm': user_filter_formdm,})


# ACA VAN LAS VISTAS PARA DESHABILITAR LOS ITEMS DE LOS LISTADOS

# Esta es la vista para deshabilitar items de las facturas manuales
@login_required
def disable_item_manInvoice(request, invoice_id):
    invoice = manInvoice.objects.get(id=invoice_id)
    invoice.enabled = not invoice.enabled
    invoice.save()
    return redirect('invoices')


# Esta es la vista para deshabilitar items de los bonos de separado
@login_required
def disable_item_sepBonuses(request, bond_id):
    bond = sepBonuses.objects.get(id=bond_id)
    bond.enabled = not bond.enabled
    bond.save()
    return redirect('sepBonuseslist')


# Esta es la vista para deshabilitar items de las descuentos por nomina
@login_required
def disable_item_payDiscount(request, payd_id):
    payd = payDiscounts.objects.get(id=payd_id)
    payd.enabled = not payd.enabled
    payd.save()
    return redirect('payDiscountslist')


# ACA VAN LAS VISTAS PARA EXPORTAR LOS LISTADOS A EXCEL

# Esta es la vista que exporta el listado de facturas manuales a excel
@login_required
def export_invoices_to_excel(request):

    print("Entrando en la función export_invoices_to_excel")

    invoices = manInvoice.objects.all()

    if request.method == 'POST':

        date_invoice_filter_form = invoice_date_filterForm(request.GET)
        user_filter_form = user_filterForm(request.GET)

        if date_invoice_filter_form.is_valid() and user_filter_form.is_valid():
            date_invoice = date_invoice_filter_form.cleaned_data.get(
                'dateInvoice')
            user_filter = user_filter_form.cleaned_data.get('user')

        date_invoice = request.POST.get('dateInvoice')
        user_filter = request.POST.get('user')

        print("Método POST detectado")
        print(f"Fecha de factura ingresada: {date_invoice}")
        print(f"Filtro de usuario ingresado: {user_filter}")

        if date_invoice or user_filter:

            try:
                date = convert_date(date_invoice)
                print(f"Fecha de factura convertida a objeto date: {date}")
                print(f"Aplicando filtro por fecha de factura: {date}")
                invoices = manInvoice.objects.filter(dateInvoice=date, enabled=True)
                print("Filtro de fecha aplicado")
            except ValueError:
                date_invoice = None

                user = user_filter
                print("Aplicando filtro por usuario")
                invoices = manInvoice.objects.filter(user__username=user, enabled=True)
                print("Filtro de usuario aplicado")

            response = generate_excel_invoices(invoices, request)
            return response


# Esta es la vista que exporta el listado de bonos de separado a excel
@login_required
def export_sepBonuses_to_excel(request):

    sepBond = sepBonuses.objects.all()

    if request.method == 'POST':

        date_invoice_filter_formsb = invoice_date_filterFormsb(request.GET)
        user_filter_formsb = user_filterFormsb(request.GET)

        if date_invoice_filter_formsb.is_valid() and user_filter_formsb.is_valid():
            date_invoice = date_invoice_filter_formsb.cleaned_data.get(
                'dateInvoice')
            user_filter = user_filter_formsb.cleaned_data.get('user')

        date_invoice = request.POST.get('dateInvoice')
        user_filter = request.POST.get('user')

        print("Método POST detectado")
        print(f"Fecha de factura ingresada: {date_invoice}")
        print(f"Filtro de usuario ingresado: {user_filter}")

        if date_invoice or user_filter:

            try:
                date = convert_date(date_invoice)
                print(f"Fecha de factura convertida a objeto date: {date}")
                print(f"Aplicando filtro por fecha de factura: {date}")
                sepBond = sepBonuses.objects.filter(
                    dateInvoice=date, enabled=True)
                print("Filtro de fecha aplicado")
            except ValueError:
                date_invoice = None

                user = user_filter
                print("Aplicando filtro por usuario")
                sepBond = sepBonuses.objects.filter(
                    user__username=user, enabled=True)
                print("Filtro de usuario aplicado")

            response = generate_excel_bonuses(sepBond, request)
            return response


# Esta es la vista que exporta el listado de descuentos por nómina a excel
@login_required
def export_payDiscounts_to_excel(request):

    payDiscount = payDiscounts.objects.all()

    if request.method == 'POST':

        date_invoice_filter_formpd = invoice_date_filterFormpd(request.GET)
        user_filter_formpd = user_filterFormpd(request.GET)

        if date_invoice_filter_formpd.is_valid() and user_filter_formpd.is_valid():
            date_invoice = date_invoice_filter_formpd.cleaned_data.get(
                'datePayDiscounts')
            user_filter = user_filter_formpd.cleaned_data.get('user')

        date_invoice = request.POST.get('datePayDiscounts')
        user_filter = request.POST.get('user')

        print("Método POST detectado")
        print(f"Fecha de factura ingresada: {date_invoice}")
        print(f"Filtro de usuario ingresado: {user_filter}")

        if date_invoice or user_filter:

            try:
                date = convert_date(date_invoice)
                print(f"Fecha de factura convertida a objeto date: {date}")
                print(f"Aplicando filtro por fecha de factura: {date}")
                payDiscount = payDiscounts.objects.filter(
                    datePayDiscounts=date, enabled=True)
                print("Filtro de fecha aplicado")
            except ValueError:
                date_invoice = None

                user = user_filter
                print("Aplicando filtro por usuario")
                payDiscount = payDiscounts.objects.filter(
                    user__username=user, enabled=True)
                print("Filtro de usuario aplicado")

        response = generate_excel_payDiscounts(payDiscount, request)
        return response

# Esta es la vista que exporta el listado de manejo de datos a excel
@login_required
def export_dataManagement_to_excel(request):

    data = dataManagement.objects.all()

    if request.method == 'POST':

        date_filter_formdm = date_filterFormdm(request.GET)
        user_filter_formdm = user_filterFormdm(request.GET)

        if date_filter_formdm.is_valid() and user_filter_formdm.is_valid():
            date_data = date_filter_formdm.cleaned_data.get('date')
            user_data = user_filter_formdm.cleaned_data.get('user')

        date_data = request.POST.get('date')
        user_data = request.POST.get('user')

        print("Método POST detectado")
        print(f"Fecha de factura ingresada: {date_data}")
        print(f"Filtro de usuario ingresado: {user_data}")

        if date_data:

            try:
                date = convert_date(date_data)
                print(f"Fecha de factura convertida a objeto date: {date}")
                print(f"Aplicando filtro por fecha de factura: {date}")
                data = dataManagement.objects.filter(created=date)
                print("Filtro de fecha aplicado")
            except ValueError:
                date_data = None

                user = user_data
                print("Aplicando filtro por usuario")
                data = dataManagement.objects.filter(user__username=user)
                print("Filtro de usuario aplicado")

        response = generate_excel_dataManagement(data, request)
        return response

