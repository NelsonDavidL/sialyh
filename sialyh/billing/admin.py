from django.contrib import admin
from .models import *

# Register your models here.

#Estos son los campos que es muestran en el listado.
class manInvoice_admin(admin.ModelAdmin):
    list_display=('numInvoice','dateInvoice','file','user')

#Aca se registra el modelo de las facturas manuales en el modulo administrativo de django
admin.site.register(manInvoice, manInvoice_admin)


#Estos son los campos que es muestran en el listado.
class sepBonuses_admin(admin.ModelAdmin):
    list_display=('numBond','valBond','dateInvoice','file','user')

#Aca se registra el modelo de las facturas manuales en el modulo administrativo de django
admin.site.register(sepBonuses, sepBonuses_admin)