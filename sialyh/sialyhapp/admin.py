from django.contrib import admin
from django.contrib.auth.models import Permission
from .models import *

# Register your models here.


class modules_admin(admin.ModelAdmin):
    list_display=('id','module')

admin.site.register(modules, modules_admin) 


class incomes_admin(admin.ModelAdmin):
    list_display=('id','incomes')

admin.site.register(incomes, incomes_admin) 

class listings_admin(admin.ModelAdmin):
    list_display=('id','listings')

admin.site.register(listings, listings_admin) 


admin.site.register(Permission)